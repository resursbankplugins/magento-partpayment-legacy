<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Setup\Patch\Data;

use Resursbank\Core\Setup\Patch\Data\RemapConfigPaths as Core;

/**
 * @inheridoc
 */
class RemapConfigPaths extends Core
{
    /**
     * @inheridoc
     * @return array<string, string>
     */
    protected function getKeys(): array
    {
        return [
            'part_payment/enabled' => 'partpayment/active',
            'part_payment/payment_method' => 'partpayment/method',
            'part_payment/duration' => 'partpayment/annuity'
        ];
    }
}
