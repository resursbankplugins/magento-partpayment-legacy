<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Block\Adminhtml\System\Config;

use Magento\Backend\Block\Template\Context;
use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Framework\View\Helper\SecureHtmlRenderer;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Core\Block\Adminhtml\System\Config\Methods\Listing;
use Resursbank\Core\Helper\Log;
use Resursbank\Core\Helper\PaymentMethods;
use Resursbank\Core\Helper\Scope;
use Resursbank\Partpayment\Helper\Method;
use Magento\Framework\App\RequestInterface;

/**
 * Render maximum monthly cost of part payment method.
 */
class Info extends Listing
{
    /**
     * @var Method
     */
    private Method $method;

    /**
     * @param Context $context
     * @param PaymentMethods $paymentMethods
     * @param Log $log
     * @param PriceCurrencyInterface $priceCurrency
     * @param RequestInterface $request
     * @param Method $method
     * @param Scope $scope
     * @param array<mixed> $data
     * @param SecureHtmlRenderer|null $secureRenderer
     */
    public function __construct(
        Context $context,
        PaymentMethods $paymentMethods,
        Log $log,
        PriceCurrencyInterface $priceCurrency,
        RequestInterface $request,
        Method $method,
        Scope $scope,
        array $data = [],
        ?SecureHtmlRenderer $secureRenderer = null
    ) {
        $this->method = $method;

        parent::__construct(
            $context,
            $paymentMethods,
            $log,
            $priceCurrency,
            $request,
            $scope,
            $data,
            $secureRenderer
        );

        $this->setTemplate('system/config/info.phtml');
    }

    /**
     * @return PaymentMethodInterface|null
     */
    public function getMethodModel(): ?PaymentMethodInterface
    {
        return $this->method->getMethod(false);
    }
}
