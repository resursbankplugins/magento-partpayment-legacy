<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Test\Unit\Helper\Annuity;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\ValidatorException;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Partpayment\Helper\Annuity\Converter;
use Resursbank\Partpayment\Model\AnnuityFactory;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 * @SuppressWarnings(PHPMD.TooManyPublicMethods)
 */
class ConverterTest extends TestCase
{
    /**
     * @var Converter
     */
    private Converter $converter;

    protected function setUp(): void
    {
        $contextMock = $this->createMock(Context::class);
        $annuityFactoryMock = $this->createMock(AnnuityFactory::class);
        $this->converter = new Converter(
            $contextMock,
            $annuityFactoryMock
        );
    }


    /**
     * Assert exception thrown on invalid data.
     * @throws \JsonException
     */
    public function testValidateThrowsExceptionWithInvalidData(): void
    {
        $this->expectException(ValidatorException::class);
        $this->expectErrorMessage(
            'Data conversion failed. Provided data is invalid. ' .
            '{"paymentPlanName":"Title","duration":123,"factor":"Factor"}'
        );
        $paymentMethodInterfaceMock = $this->createMock(PaymentMethodInterface::class);
        $data = [];
        $data[Converter::KEY_TITLE] = 'Title';
        $data[Converter::KEY_DURATION] = 123;
        $data[Converter::KEY_FACTOR] = 'Factor';
        $this->converter->convert($data, $paymentMethodInterfaceMock);
    }

    /**
     * Assert successful validation with int value.
     */
    public function testValidateReturnsTrueWithCorrectData(): void
    {
        $data = [];
        $data[Converter::KEY_TITLE] = 'Title';
        $data[Converter::KEY_DURATION] = 123;
        $data[Converter::KEY_FACTOR] = 123.12;
        self::assertTrue($this->converter->validate($data));
    }

    /**
     * Assert successful validation of Duration with int value.
     */
    public function testValidateDurationReturnsTrueWithCorrectData(): void
    {
        $data = [];
        $data[Converter::KEY_DURATION] = 123;
        self::assertTrue($this->converter->validateDuration($data));
    }

    /**
     * Assert failed validation on string value.
     */
    public function testValidateDurationReturnsFalseWithStringValue(): void
    {
        $data = [];
        $data[Converter::KEY_DURATION] = '123';
        self::assertFalse($this->converter->validateDuration($data));
    }

    /**
     * Assert failed validation on float value.
     */
    public function testValidateDurationReturnsFalseWithFloatValue(): void
    {
        $data = [];
        $data[Converter::KEY_DURATION] = 123.12;
        self::assertFalse($this->converter->validateDuration($data));
    }

    /**
     * Assert successful validation of Factor with int value.
     */
    public function testValidateFactorReturnsTrueWithCorrectData(): void
    {
        $data = [];
        $data[Converter::KEY_FACTOR] = 123;
        self::assertTrue($this->converter->validateFactor($data));
    }

    /**
     * Assert successful validation on string value.
     */
    public function testValidateFactorReturnsTrueWithStringNumericValue(): void
    {
        $data = [];
        $data[Converter::KEY_FACTOR] = '123';
        self::assertTrue($this->converter->validateFactor($data));
    }


    /**
     * Assert successful validation on string value.
     */
    public function testValidateFactorReturnsTrueWithStringFloatValue(): void
    {
        $data = [];
        $data[Converter::KEY_FACTOR] = '123.45';
        self::assertTrue($this->converter->validateFactor($data));
    }

    /**
     * Assert successful validation on float value.
     */
    public function testValidateFactorReturnsTrueWithFloatValue(): void
    {
        $data = [];
        $data[Converter::KEY_FACTOR] = 123.12;
        self::assertTrue($this->converter->validateFactor($data));
    }

    /**
     * Assert successful validation on string value.
     */
    public function testValidateTitleReturnsTrueWithCorrectData(): void
    {
        $data = [];
        $data[Converter::KEY_TITLE] = 'title';
        self::assertTrue($this->converter->validateTitle($data));
    }

    /**
     * Assert successful validation on string value even if numeric.
     */
    public function testValidateTitleReturnsTrueWithStringValue(): void
    {
        $data = [];
        $data[Converter::KEY_TITLE] = '123';
        self::assertTrue($this->converter->validateTitle($data));
    }

    /**
     * Assert failed validation on float value.
     */
    public function testValidateTitleReturnsFalseWithFloatValue(): void
    {
        $data = [];
        $data[Converter::KEY_TITLE] = 123.12;
        self::assertFalse($this->converter->validateTitle($data));
    }

    /**
     * Assert failed validation on int value.
     */
    public function testValidateTitleReturnsFalseWithIntValue(): void
    {
        $data = [];
        $data[Converter::KEY_TITLE] = 123;
        self::assertFalse($this->converter->validateTitle($data));
    }
}
