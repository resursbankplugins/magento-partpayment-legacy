<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Test\Unit\Helper;

use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Api\Data\StoreInterface;
use Magento\Store\Model\StoreManagerInterface;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Core\Helper\Scope;
use Resursbank\Core\Model\PaymentMethodRepository;
use Resursbank\Partpayment\Helper\Config;
use Resursbank\Partpayment\Helper\Log;
use Resursbank\Partpayment\Helper\Method;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class MethodTest extends TestCase
{
    /**
     * @var Config|MockObject
     */
    private $configMock;

    /**
     * @var Log|MockObject
     */
    private $logMock;

    /**
     * @var PaymentMethodRepository|MockObject
     */
    private $methodRepositoryMock;

    /**
     * @var StoreManagerInterface|MockObject
     */
    private $storeManagerMock;

    /**
     * @var Scope|MockObject
     */
    private $scopeMock;

    /**
     * @var Method
     */
    private Method $method;

    /**
     * @inheriDoc
     */
    public function setUp(): void
    {
        $contextMock = $this->createMock(Context::class);
        $this->configMock = $this->createMock(Config::class);
        $this->logMock = $this->createMock(Log::class);
        $this->methodRepositoryMock = $this->createMock(PaymentMethodRepository::class);
        $this->storeManagerMock = $this->createMock(StoreManagerInterface::class);
        $this->scopeMock = $this->createMock(Scope::class);
        $this->method = new Method(
            $contextMock,
            $this->configMock,
            $this->logMock,
            $this->methodRepositoryMock,
            $this->storeManagerMock,
            $this->scopeMock
        );
    }


    /**
     * Assert that getMethod returns class of PaymentMethodInterface.
     */
    public function testGetMethodReturnsCorrectClass(): void
    {
        $this->scopeMock->expects(self::once())->method('getId')->willReturn('0');
        $this->configMock->expects(self::once())->method('getMethod')->willReturn(1);
        $paymentMethodInterfaceMock = $this->createMock(PaymentMethodInterface::class);
        $this->methodRepositoryMock
            ->expects(self::once())
            ->method('get')
            ->with(1)
            ->willReturn($paymentMethodInterfaceMock);
        $this->logMock->expects(self::never())->method('exception');

        self::assertInstanceOf(PaymentMethodInterface::class, $this->method->getMethod(false));
    }

    /**
     * Assert that getMethod gets paymentMethod from store manager code if $frontend = true.
     */
    public function testGetMethodRetrieveTheCorrectPaymentMethod(): void
    {
        $storeMock = $this->createMock(StoreInterface::class);
        $paymentMethodInterfaceMock = $this->createMock(PaymentMethodInterface::class);

        $this->storeManagerMock->expects(self::once())->method('getStore')->willReturn($storeMock);
        $storeMock->expects(self::once())->method('getCode')->willReturn('en');
        $this->configMock->expects(self::once())->method('getMethod')->with('en')->willReturn(1);

        $this->methodRepositoryMock->method('get')->with(1)->willReturn($paymentMethodInterfaceMock);
        $this->logMock->expects(self::never())->method('exception');
        $this->scopeMock->expects(self::never())->method('getId');

        self::assertInstanceOf(PaymentMethodInterface::class, $this->method->getMethod(true));
    }

    /**
     * Assert that getMethod returns null and not throws error if PaymentMethod doesn't exist.
     */
    public function testGetMethodThrowsNoSuchEntityException(): void
    {
        $this->scopeMock->method('getId')->willReturn('0');
        $this->configMock->method('getMethod')->willReturn(1);
        $this->methodRepositoryMock->method('get')->with(1)->willThrowException(new NoSuchEntityException());
        $this->logMock->expects(self::once())->method('exception');
        self::assertNull($this->method->getMethod(false));
    }

    /**
     * Assert that getMethod returns true if eligible.
     * @throws \JsonException
     */
    public function testIsEligibleReturnsTrueWhenEligible(): void
    {
        $paymentMethodInterfaceMock = $this->createMock(PaymentMethodInterface::class);
        $paymentMethodInterfaceMock->expects(self::once())->method('getRaw')->with('')->willReturn('{
            "specificType":"PART_PAYMENT"
        }');
        self::assertTrue($this->method->isEligible($paymentMethodInterfaceMock));
    }

    /**
     * Assert that getMethod returns false if not eligible.
     * @throws \JsonException
     */
    public function testIsEligibleReturnsFalseWhenNotEligible(): void
    {
        $paymentMethodInterfaceMock = $this->createMock(PaymentMethodInterface::class);
        $paymentMethodInterfaceMock->expects(self::once())->method('getRaw')->with('')->willReturn('{
            "specificType":"SOMETHING_ELSE"
        }');
        self::assertFalse($this->method->isEligible($paymentMethodInterfaceMock));
    }

    /**
     * Assert that getMethod throws JsonException on corrupt json.
     * @throws \JsonException
     */
    public function testIsEligibleThrowsExceptionOnCorruptJson(): void
    {
        $this->expectException(\JsonException::class);

        $paymentMethodInterfaceMock = $this->createMock(PaymentMethodInterface::class);
        $paymentMethodInterfaceMock->expects(self::once())->method('getRaw')->with('')->willReturn('{
            "specificType":"SOMETHING_ELSE
        }');
        $this->method->isEligible($paymentMethodInterfaceMock);
    }
}
