/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

// phpcs:ignoreFile
define(
    [
        'ko',
        'jquery',
        'mage/translate',
        'uiComponent',
        'priceBox',
        'uiLayout',
        'Magento_Catalog/js/price-utils',
        'Resursbank_Core/js/lib/product',
        'Resursbank_Core/js/lib/remodal',
        'Resursbank_Core/js/remodal',
        'Resursbank_Partpayment/js/lib/part-payment',
        'Resursbank_Partpayment/js/model/part-payment'
    ],
    /**
     *
     * @param ko
     * @param $
     * @param $t
     * @param Component
     * @param PriceBox
     * @param Layout
     * @param PriceUtils
     * @param {RbC.Lib.Product} ProductLib
     * @param {RbC.Lib.Remodal} RemodalLib
     * @param Remodal
     * @param {RbPp.Lib.PartPayment} PartPaymentLib
     * @param {RbPp.Model.PartPayment} Model
     * @returns {*}
     */
    function(
        ko,
        $,
        $t,
        Component,
        PriceBox,
        Layout,
        PriceUtils,
        ProductLib,
        RemodalLib,
        Remodal,
        PartPaymentLib,
        Model
    ) {
        'use strict';

        return Component.extend({
            defaults: {
                template: 'Resursbank_Partpayment/catalog/product/part-payment'
            },

            /**
             * @param {object} args
             * @param {number} args.duration
             * @param {number} args.factor
             * @param {number} args.suggestedPrice
             * @param {string} args.formKey
             * @param {string} args.productType
             */
            initialize: function (args) {
                var me = this;

                me._super();

                Model.init({
                    duration: args.duration,
                    factor: args.factor,
                    formKey: args.formKey,
                    productType: args.productType,
                    suggestedPrice: args.suggestedPrice,
                    finalPrice: ProductLib.getFinalPrice(args.productType)
                });

                /**
                 * @type {RbC.Ko.String}
                 */
                me.suggestedPrice = ko.computed(function() {
                    var priceFormat = ProductLib.getPriceFormat(
                        args.productType
                    );

                    return PriceUtils.formatPrice(
                        Model.suggestedPrice(),
                        priceFormat !== null ? priceFormat : {}
                    );
                });

                /**
                 * @type {string}
                 */
                me.partPaymentLabel = ko.observable($t(
                    'Pay in installments with Resurs Bank'
                ));

                /**
                 * The duration of the part payment.
                 *
                 * @type {RbC.Ko.String}
                 */
                me.partPaymentInfo = ko.computed(function () {
                    return $t('Starting at %1 per month, for %2 months')
                        .replace('%1', me.suggestedPrice())
                        .replace('%2', Model.duration());
                });

                /**
                 * Path to the logo of a credit card payment method.
                 *
                 * @type {string}
                 */
                me.logo = require.toUrl(
                    'Resursbank_Partpayment/images/resurs-bank-logo.png'
                );

                (function init() {
                    ProductLib.getPriceBoxInstance(args.productType).element.on(
                        'updatePrice',
                        function () {
                            Model.finalPrice(
                                ProductLib.getFinalPrice(args.productType)
                            );

                            Model.suggestedPrice(
                                ProductLib.getSuggestedPrice(
                                    Model.finalPrice(),
                                    args.factor
                                )
                            );
                        });

                    Layout([{
                        parent: me.name,
                        name: me.name + '.read-more-link',
                        displayArea: 'read-more-link',
                        component: 'Resursbank_Core/js/view/read-more',
                        config: {
                            remodalId: me.remodalId,
                            label: me.partPaymentLabel,
                            info: me.partPaymentInfo,
                            modalTitle: 'Resurs Bank - ' + $t('Part Payment'),
                            modalComponent: 'Resursbank_Partpayment/js/view/catalog/product/remodal',
                            requestFn: function () {
                                return PartPaymentLib.getCostOfPurchase(
                                    Model.finalPrice()
                                );
                            }
                        }
                    }]);
                }());
            }
        });
    }
);
