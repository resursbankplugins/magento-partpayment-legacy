# Resurs Bank - Magento 2 module - Part Payment

## Description

Part payment functionality for Magento 2 module.

---

## Prerequisites

* [Magento 2](https://devdocs.magento.com/guides/v2.4/install-gde/bk-install-guide.html) [Supports Magento 2.4.1+]

---

#### 1.5.1

* Removed default threshold value.

#### 1.6.3

* Replaced logotypes.
* Added PHP 8.1 support.
* Fixed faulty business logic to display product page widget.

#### 1.6.4

* Updated som tests.
