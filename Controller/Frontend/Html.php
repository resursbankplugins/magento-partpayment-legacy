<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Controller\Frontend;

use Magento\Framework\App\RequestInterface;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Core\Controller\Frontend\ReadMore;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Core\Model\PaymentMethodRepository;
use Resursbank\Core\Helper\Api\Credentials;
use Resursbank\Core\Helper\Api;
use Resursbank\Partpayment\Helper\Log;
use Resursbank\Partpayment\Helper\Method;
use function is_string;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Html extends ReadMore
{
    /**
     * @var Method
     */
    private Method $method;

    /**
     * @param Log $log
     * @param RequestInterface $request
     * @param ResultFactory $resultFactory
     * @param Method $method
     * @param PaymentMethodRepository $methodRepository
     * @param Api $api
     * @param Credentials $credentials
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Log $log,
        RequestInterface $request,
        ResultFactory $resultFactory,
        Method $method,
        PaymentMethodRepository $methodRepository,
        Api $api,
        Credentials $credentials,
        StoreManagerInterface $storeManager
    ) {
        $this->method = $method;

        parent::__construct(
            $log,
            $request,
            $resultFactory,
            $methodRepository,
            $api,
            $credentials,
            $storeManager
        );
    }

    /**
     * @inheridoc
     * @throws InvalidDataException
     * @throws NoSuchEntityException
     * @noinspection PhpMissingParentCallCommonInspection
     */
    protected function getMethod(): PaymentMethodInterface
    {
        $code = $this->request->getParam('code');

        $method = (is_string($code) && !empty($code)) ?
            $this->methodRepository->getByCode($code) :
            $this->method->getMethod(true);

        if ($method === null) {
            throw new InvalidDataException(
                __('Unable to obtain payment method.')
            );
        }

        return $method;
    }
}
