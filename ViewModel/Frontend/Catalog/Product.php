<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\ViewModel\Frontend\Catalog;

use function is_string;
use Exception;
use Magento\Store\Model\StoreManagerInterface;
use Magento\Catalog\Api\Data\ProductInterface;
use Magento\Catalog\Model\Product as CatalogProduct;
use Magento\Catalog\Api\ProductRepositoryInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Pricing\PriceInfo\Base;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Partpayment\Helper\Annuity;
use Resursbank\Partpayment\Helper\Config;
use Resursbank\Partpayment\Helper\Log;
use Resursbank\Partpayment\Helper\Method;
use Resursbank\Partpayment\Helper\Price;
use Resursbank\Partpayment\ViewModel\Frontend\AbstractModel;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Product extends AbstractModel
{
    /**
     * @var RequestInterface
     */
    private RequestInterface $request;

    /**
     * @var ProductRepositoryInterface
     */
    private ProductRepositoryInterface $productRepository;

    /**
     * @var ProductInterface|null
     */
    private ?ProductInterface $productModel = null;

    /**
     * @param Log $log
     * @param Config $config
     * @param FormKey $formKey
     * @param Price $price
     * @param Method $method
     * @param RequestInterface $request
     * @param ProductRepositoryInterface $productRepository
     * @param Annuity $annuity
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Log $log,
        Config $config,
        FormKey $formKey,
        Price $price,
        Method $method,
        RequestInterface $request,
        ProductRepositoryInterface $productRepository,
        Annuity $annuity,
        StoreManagerInterface $storeManager
    ) {
        $this->request = $request;
        $this->productRepository = $productRepository;

        parent::__construct(
            $log,
            $config,
            $formKey,
            $price,
            $method,
            $annuity,
            $storeManager
        );
    }

    /**
     * Returns the price of the product, or 0.0 if the product can't be found.
     *
     * @return float
     */
    public function getPrice(): float
    {
        $result = 0.0;

        try {
            $product = $this->getProduct();
            $result = (float) $product->getPrice();

            if ($product instanceof CatalogProduct) {
                $priceInfo = $product->getPriceInfo();

                if ($priceInfo instanceof Base) {
                    $result = (float) $priceInfo
                        ->getPrices()
                        ->get('final_price')
                        ->getAmount()
                        ->getValue();
                }
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getProductType(): string
    {
        $result = '';

        try {
            // Avoid type casting since getTypeId may return an array.
            $result = $this->getProduct()->getTypeId();
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return is_string($result) ? $result : '';
    }

    /**
     * @return ProductInterface
     * @throws InvalidDataException
     * @throws NoSuchEntityException
     */
    private function getProduct(): ProductInterface
    {
        $product = $this->productModel;

        if ($product === null) {
            $productId = (int)$this->request->getParam('id');

            if ($productId > 0) {
                /** @var mixed $storeId */
                $storeId = $this->storeManager->getStore()->getId();
                $product = $this->productRepository->getById(
                    $productId,
                    false,
                    is_string($storeId) ? (int)$storeId : null
                );
            }

            $this->productModel = $product;
        }

        if ($product === null) {
            throw new InvalidDataException(__('Unable to load product.'));
        }

        return $product;
    }
}
