<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\ViewModel\Frontend;

use Exception;
use Magento\Framework\Exception\NoSuchEntityException;
use Magento\Framework\Data\Form\FormKey;
use Magento\Framework\View\Element\Block\ArgumentInterface;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Partpayment\Api\Data\AnnuityInterface;
use Resursbank\Partpayment\Helper\Config;
use Resursbank\Partpayment\Helper\Log;
use Resursbank\Partpayment\Helper\Method;
use Resursbank\Partpayment\Helper\Annuity;
use Resursbank\Partpayment\Helper\Price;

abstract class AbstractModel implements ArgumentInterface
{
    /**
     * @var FormKey
     */
    protected FormKey $formKey;

    /**
     * @var Log
     */
    protected Log $log;

    /**
     * @var Config
     */
    protected Config $config;

    /**
     * @var Price
     */
    protected Price $price;

    /**
     * @var Method
     */
    protected Method $method;

    /**
     * @var PaymentMethodInterface|null
     */
    protected ?PaymentMethodInterface $methodModel = null;

    /**
     * @var AnnuityInterface|null
     */
    protected ?AnnuityInterface $annuityModel = null;

    /**
     * @var Annuity
     */
    private Annuity $annuity;

    /**
     * @var StoreManagerInterface
     */
    protected StoreManagerInterface $storeManager;

    /**
     * @param Log $log
     * @param Config $config
     * @param FormKey $formKey
     * @param Price $price
     * @param Method $method
     * @param Annuity $annuity
     * @param StoreManagerInterface $storeManager
     */
    public function __construct(
        Log $log,
        Config $config,
        FormKey $formKey,
        Price $price,
        Method $method,
        Annuity $annuity,
        StoreManagerInterface $storeManager
    ) {
        $this->log = $log;
        $this->formKey = $formKey;
        $this->config = $config;
        $this->price = $price;
        $this->method = $method;
        $this->annuity = $annuity;
        $this->storeManager = $storeManager;
    }

    /**
     * Retrieve form key value, for AJAX requests.
     *
     * @return string
     */
    public function getFormKey(): string
    {
        $result = '';

        try {
            $result = $this->formKey->getFormKey();
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Check whether module is active.
     *
     * @param float $price
     * @return bool
     */
    public function isActive(
        float $price
    ): bool {
        $result = false;

        try {
            $active = $this->config->isActive(
                $this->storeManager->getStore()->getCode()
            );

            if ($active) {
                $method = $this->getMethod();
                $result = $this->price->isValid(
                    $method,
                    $this->getAnnuity(),
                    $price
                );
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Retrieve suggested part payment price based on Quote total.
     *
     * @param float $price
     * @return float
     */
    public function getSuggestedPrice(
        float $price
    ): float {
        $result = 0.0;

        try {
            $method = $this->getMethod();
            $result = $this->price->getSuggestedPrice(
                $method,
                $this->getAnnuity(),
                $price
            );
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Retrieve duration of configured annuity to base calculations on.
     *
     * @return int
     */
    public function getDuration(): int
    {
        $result = 0;

        try {
            $result = (int) $this->getAnnuity()->getDuration();
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Retrieve factor of configured annuity to base calculations on.
     *
     * @return float
     */
    public function getFactor(): float
    {
        $result = 0.0;

        try {
            $result = (float) $this->getAnnuity()->getFactor();
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Retrieve configured payment method to base calculations on. Store in
     * local variable to suppress redundant database transactions.
     *
     * @return PaymentMethodInterface
     * @throws InvalidDataException
     */
    public function getMethod(): PaymentMethodInterface
    {
        $method = $this->methodModel;

        if ($method === null) {
            $method = $this->method->getMethod(true);

            if ($method === null) {
                throw new InvalidDataException(
                    __('Please configure payment method to base prices on')
                );
            }

            $this->methodModel = $method;
        }

        return $method;
    }

    /**
     * Retrieve configured annuity to base calculations on. Store in local
     * variable to suppress redundant database transactions.
     *
     * @return AnnuityInterface
     * @throws InvalidDataException|NoSuchEntityException
     */
    public function getAnnuity(): AnnuityInterface
    {
        $annuity = $this->annuityModel;

        if ($annuity === null) {
            $annuity = $this->annuity->getAnnuity(
                $this->storeManager->getStore()->getCode()
            );

            if ($annuity === null) {
                throw new InvalidDataException(
                    __('Please configure duration to base prices on.')
                );
            }

            $this->annuityModel = $annuity;
        }

        return $annuity;
    }
}
