<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Helper;

use function array_key_exists;
use Exception;
use function is_array;
use JsonException;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Core\Model\PaymentMethodRepository;
use Resursbank\Core\Helper\Scope;

class Method extends AbstractHelper
{
    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var PaymentMethodRepository
     */
    private PaymentMethodRepository $methodRepository;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var Scope
     */
    private Scope $scope;

    /**
     * @param Context $context
     * @param Config $config
     * @param Log $log
     * @param PaymentMethodRepository $methodRepository
     * @param StoreManagerInterface $storeManager
     * @param Scope $scope
     */
    public function __construct(
        Context $context,
        Config $config,
        Log $log,
        PaymentMethodRepository $methodRepository,
        StoreManagerInterface $storeManager,
        Scope $scope
    ) {
        $this->config = $config;
        $this->log = $log;
        $this->methodRepository = $methodRepository;
        $this->storeManager = $storeManager;
        $this->scope = $scope;

        parent::__construct($context);
    }

    /**
     * Retrieve payment method model of configured payment method to be utilised
     * in part payment price calculations.
     *
     * @param bool $frontend
     * @return null|PaymentMethodInterface
     */
    public function getMethod(
        bool $frontend
    ): ?PaymentMethodInterface {
        $result = null;

        try {
            $methodId = $this->config->getMethod(
                $frontend ?
                    $this->storeManager->getStore()->getCode() :
                    $this->scope->getId()
            );

            if ($methodId !== 0) {
                $result = $this->methodRepository->get($methodId);
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Check if a payment method is eligible to be utilised for part payments.
     *
     * @param PaymentMethodInterface $method
     * @return bool
     * @throws JsonException
     */
    public function isEligible(
        PaymentMethodInterface $method
    ): bool {
        $raw = json_decode(
            (string) $method->getRaw(),
            true,
            512,
            JSON_THROW_ON_ERROR
        );

        return (
            is_array($raw) &&
            array_key_exists('specificType', $raw) &&
            (
                $raw['specificType'] === 'PART_PAYMENT' ||
                $raw['specificType'] === 'REVOLVING_CREDIT' ||
                $raw['specificType'] === 'CARD'
            )
        );
    }
}
