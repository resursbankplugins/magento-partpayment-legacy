<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Helper;

use Magento\Store\Model\ScopeInterface;
use Resursbank\Core\Helper\AbstractConfig;

class Config extends AbstractConfig
{
    /**
     * @var string
     */
    public const GROUP = 'partpayment';

    /**
     * @param string|null $scopeCode
     * @param string $scopeType
     * @return bool
     */
    public function isActive(
        ?string $scopeCode,
        string $scopeType = ScopeInterface::SCOPE_STORE
    ): bool {
        return $this->isEnabled(
            self::GROUP,
            'active',
            $scopeCode,
            $scopeType
        );
    }

    /**
     * @param string $scopeCode
     * @param string $scopeType
     * @return bool
     */
    public function isDebugEnabled(
        string $scopeCode,
        string $scopeType = ScopeInterface::SCOPE_STORE
    ): bool {
        return $this->isEnabled(
            self::GROUP,
            'debug',
            $scopeCode,
            $scopeType
        );
    }

    /**
     * @param null|string $scopeCode
     * @param string $scopeType
     * @return int
     */
    public function getMethod(
        ?string $scopeCode,
        string $scopeType = ScopeInterface::SCOPE_STORE
    ): int {
        return (int) $this->get(
            self::GROUP,
            'method',
            $scopeCode,
            $scopeType
        );
    }

    /**
     * @param int $scopeId
     * @param string $scopeType
     * @return void
     */
    public function deleteMethod(
        int $scopeId,
        string $scopeType = ScopeInterface::SCOPE_STORE
    ): void {
        $this->delete(
            self::GROUP,
            'method',
            $scopeId,
            $scopeType
        );
    }

    /**
     * @param string|null $scopeCode
     * @param string $scopeType
     * @return int
     */
    public function getAnnuity(
        ?string $scopeCode,
        string $scopeType = ScopeInterface::SCOPE_STORE
    ): int {
        return (int) $this->get(
            self::GROUP,
            'annuity',
            $scopeCode,
            $scopeType
        );
    }

    /**
     * @param string|null $scopeCode
     * @param string $scopeType
     * @return float
     */
    public function getThreshold(
        ?string $scopeCode,
        string $scopeType = ScopeInterface::SCOPE_STORE
    ): float {
        return (float) $this->get(
            self::GROUP,
            'threshold',
            $scopeCode,
            $scopeType
        );
    }
}
