<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Helper;

use Exception;
use Magento\Framework\Api\SearchCriteriaBuilder;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\AlreadyExistsException;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Core\Exception\InvalidDataException;
use Resursbank\Partpayment\Api\Data\AnnuityInterface;
use Resursbank\Partpayment\Model\AnnuityRepository;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Annuity extends AbstractHelper
{
    /**
     * @var Config
     */
    private Config $config;

    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var AnnuityRepository
     */
    private AnnuityRepository $repository;

    /**
     * @var SearchCriteriaBuilder
     */
    private SearchCriteriaBuilder $searchBuilder;

    /**
     * @param Context $context
     * @param Config $config
     * @param Log $log
     * @param AnnuityRepository $repository
     * @param SearchCriteriaBuilder $searchBuilder
     */
    public function __construct(
        Context $context,
        Config $config,
        Log $log,
        AnnuityRepository $repository,
        SearchCriteriaBuilder $searchBuilder
    ) {
        $this->config = $config;
        $this->log = $log;
        $this->repository = $repository;
        $this->searchBuilder = $searchBuilder;

        parent::__construct($context);
    }

    /**
     * Retrieve annuity model of configured annuity to be utilised in part
     * payment price calculations.
     *
     * @param string $scopeCode
     * @return null|AnnuityInterface
     */
    public function getAnnuity(
        string $scopeCode
    ): ?AnnuityInterface {
        $result = null;

        try {
            $annuityId = $this->config->getAnnuity($scopeCode);

            if ($annuityId !== 0) {
                $result = $this->repository->get($annuityId);
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * @param PaymentMethodInterface $method
     * @return AnnuityInterface[]
     */
    public function getListByMethod(
        PaymentMethodInterface $method
    ): array {
        $searchCriteria = $this->searchBuilder
            ->addFilter(AnnuityInterface::METHOD_ID, $method->getMethodId())
            ->create();

        return $this->repository->getList($searchCriteria)->getItems();
    }

    /**
     * @param array<int> $ids
     * @param PaymentMethodInterface $method
     * @return AnnuityInterface[]
     */
    public function getListExcluding(
        array $ids,
        PaymentMethodInterface $method
    ): array {
        $searchCriteria = $this->searchBuilder
            ->addFilter(AnnuityInterface::METHOD_ID, $method->getMethodId())
            ->addFilter(AnnuityInterface::ANNUITY_ID, $ids, 'nin')
            ->create();

        return $this->repository
            ->getList($searchCriteria)
            ->getItems();
    }

    /**
     * @param AnnuityInterface $annuity
     * @throws Exception
     */
    public function delete(
        AnnuityInterface $annuity
    ): void {
        if ($annuity->getAnnuityId() !== null) {
            $this->repository->delete($annuity);
        }
    }

    /**
     * Retrieve AnnuityInterface matching the supplied one, to check if it
     * already exists in the database table.
     *
     * @param AnnuityInterface $annuity
     * @param PaymentMethodInterface $method
     * @return AnnuityInterface|null
     */
    public function getDuplicate(
        AnnuityInterface $annuity,
        PaymentMethodInterface $method
    ): ?AnnuityInterface {
        $searchCriteria = $this->searchBuilder
            ->addFilter(AnnuityInterface::METHOD_ID, $method->getMethodId())
            ->addFilter(AnnuityInterface::DURATION, $annuity->getDuration())
            ->create();

        $list = $this->repository->getList($searchCriteria)->getItems();

        return !empty($list) ? end($list) : null;
    }

    /**
     * @param AnnuityInterface $annuity
     * @param PaymentMethodInterface $method
     * @return AnnuityInterface
     * @throws AlreadyExistsException
     * @throws InvalidDataException
     */
    public function update(
        AnnuityInterface $annuity,
        PaymentMethodInterface $method
    ): AnnuityInterface {
        $duplicate = $this->getDuplicate($annuity, $method);

        // Update existing entry, or create a new entry.
        if ($duplicate !== null) {
            /** @noinspection PhpUndefinedMethodInspection */
            $result = $this->repository->save(
                /** @phpstan-ignore-next-line */
                $duplicate->addData($annuity->getData())
            );
        } else {
            $result = $this->repository->save($annuity);
        }

        if (!($result instanceof AnnuityInterface)) {
            throw new InvalidDataException(
                __('Failed to update/create annuity entity.')
            );
        }

        return $result;
    }
}
