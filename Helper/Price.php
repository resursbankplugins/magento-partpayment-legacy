<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Helper;

use Exception;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Store\Model\StoreManagerInterface;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Partpayment\Api\Data\AnnuityInterface;
use Magento\Framework\Pricing\PriceCurrencyInterface;

class Price extends AbstractHelper
{
    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var PriceCurrencyInterface
     */
    private PriceCurrencyInterface $priceCurrency;

    /**
     * @var StoreManagerInterface
     */
    private StoreManagerInterface $storeManager;

    /**
     * @var Config
     */
    private Config $config;

    /**
     * @param Context $context
     * @param Log $log
     * @param PriceCurrencyInterface $priceCurrency
     * @param StoreManagerInterface $storeManager
     * @param Config $config
     */
    public function __construct(
        Context $context,
        Log $log,
        PriceCurrencyInterface $priceCurrency,
        StoreManagerInterface $storeManager,
        Config $config
    ) {
        $this->log = $log;
        $this->priceCurrency = $priceCurrency;
        $this->storeManager = $storeManager;
        $this->config = $config;

        parent::__construct($context);
    }

    /**
     * Retrieve calculated part payment price.
     *
     * @param PaymentMethodInterface $method
     * @param AnnuityInterface $annuity
     * @param float $price
     * @return float
     */
    public function calculate(
        PaymentMethodInterface $method,
        AnnuityInterface $annuity,
        float $price
    ): float {
        $result = 0;

        if ($annuity->getFactor() > 0) {
            try {
                if ($this->isValid($method, $annuity, $price)) {
                    $value = (($price * $annuity->getFactor()) * 100);
                    $result = round($value / 100, 2);
                }
            } catch (Exception $e) {
                $this->log->exception($e);
            }
        }

        return $result;
    }

    /**
     * Check if price is eligible for part payment plans.
     *
     * @param PaymentMethodInterface $method
     * @param AnnuityInterface $annuity
     * @param float $price
     * @return bool
     */
    public function isValid(
        PaymentMethodInterface $method,
        AnnuityInterface $annuity,
        float $price
    ): bool {
        $calculatedPrice = $price * $annuity->getFactor();

        return (
            $calculatedPrice >= $this->getThreshold() &&
            ($price >= $method->getMinOrderTotal()) &&
            ($price <= $method->getMaxOrderTotal())
        );
    }

    /**
     * Retrieve suggested part payment price based on product price.
     *
     * @param PaymentMethodInterface $method
     * @param AnnuityInterface $annuity
     * @param float $price
     * @return float
     */
    public function getSuggestedPrice(
        PaymentMethodInterface $method,
        AnnuityInterface $annuity,
        float $price
    ): float {
        $result = 0.0;

        try {
            if ($price > 0.0) {
                $result = ceil($this->calculate($method, $annuity, $price));
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }

    /**
     * Returns a formatted product price.
     *
     * @param float $amount
     * @return string
     */
    public function format(
        float $amount
    ): string {
        return $this->priceCurrency->convertAndFormat($amount, false, 0);
    }

    /**
     * @return float
     */
    public function getThreshold(): float
    {
        $result = 150.00;

        try {
            $result = $this->config->getThreshold(
                $this->storeManager->getStore()->getCode()
            );
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }
}
