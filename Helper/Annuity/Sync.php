<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Helper\Annuity;

use Exception;
use function is_array;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\IntegrationException;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Core\Helper\Api;
use Resursbank\Core\Model\Api\Credentials;
use Resursbank\Partpayment\Api\Data\AnnuityInterface;
use Resursbank\Partpayment\Helper\Annuity;
use Resursbank\Partpayment\Helper\Log;
use stdClass;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Sync extends AbstractHelper
{
    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var Api
     */
    private Api $api;

    /**
     * @var Converter
     */
    private Converter $converter;

    /**
     * @var Annuity
     */
    private Annuity $annuity;
    /**
     * @param Context $context
     * @param Log $log
     * @param Api $api
     * @param Converter $converter
     * @param Annuity $annuity
     */
    public function __construct(
        Context $context,
        Log $log,
        Api $api,
        Converter $converter,
        Annuity $annuity
    ) {
        $this->log = $log;
        $this->api = $api;
        $this->converter = $converter;
        $this->annuity = $annuity;

        parent::__construct($context);
    }

    /**
     * Updates database entries of annuities for a specific payment method with
     * data fetched from the API.
     *
     * If this process somehow fails for an annuity, an error will be logged and
     * that annuity will be ignored.
     *
     * Returns a list of IDs (integers) that were successfully synced.
     *
     * @param PaymentMethodInterface $method
     * @param Credentials $credentials
     * @throws Exception
     * @return array<int>
     */
    public function sync(
        PaymentMethodInterface $method,
        Credentials $credentials
    ): array {
        /** @var array<int> $synced */
        $synced = [];

        // Load annuities from the API.
        $annuities = $this->api
            ->getConnection($credentials)
            ->getAnnuityFactors((string)$method->getIdentifier());

        foreach ($annuities as $annuity) {
            try {
                // New entry from converted data.
                $converted = $this->arrayToAnnuity($annuity, $method);

                if ($converted !== null) {
                    $annuityId = $this->annuity
                        ->update($converted, $method)
                        ->getAnnuityId();

                    if (is_int($annuityId)) {
                        $synced[] = $annuityId;
                    }
                    
                    $this->log->info(
                        'Synced annuity ' .
                        '"' . $converted->getRaw() . '"' .
                        'for payment method ' .
                        '"' . $method->getCode() . '"'
                    );
                }
            } catch (Exception $error) {
                $this->log->exception($error);
            }
        }

        return $synced;
    }

    /**
     * Convert anonymous array to AnnuityInterface instance.
     *
     * @param mixed $data
     * @param PaymentMethodInterface $method
     * @return AnnuityInterface|null
     */
    public function arrayToAnnuity(
        $data,
        PaymentMethodInterface $method
    ): ?AnnuityInterface {
        $annuity = null;

        try {
            $annuity = $this->converter->convert(
                $this->resolveAnnuityDataArray($data),
                $method
            );
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $annuity;
    }

    /**
     * The data returned from ECom when fetching annuities is described as
     * mixed. We can therefore not be certain what we get back and need to
     * properly convert the data to an array for further processing.
     *
     * @param mixed $data
     * @return array<string, mixed>
     * @throws IntegrationException
     */
    public function resolveAnnuityDataArray(
        $data
    ): array {
        $result = $data;

        if ($data instanceof stdClass) {
            $result = (array) $result;
        }

        if (!is_array($result)) {
            throw new IntegrationException(
                __('Unexpected annuity returned from ECom.')
            );
        }

        return $result;
    }

    /**
     * Takes a list of annuity IDs, then removes all entries from the database
     * that do not have any of the given IDs. In other words, all of the
     * IDs in the list represents entries that will not be deleted.
     *
     * @param array<int> $ids
     * @param PaymentMethodInterface $method
     * @throws Exception
     */
    public function deleteOldEntries(
        array $ids,
        PaymentMethodInterface $method
    ): void {
        foreach ($this->annuity->getListExcluding($ids, $method) as $annuity) {
            $this->annuity->delete($annuity);
        }
    }
}
