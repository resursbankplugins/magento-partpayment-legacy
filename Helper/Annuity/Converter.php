<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Helper\Annuity;

use JsonException;
use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Magento\Framework\Exception\ValidatorException;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Partpayment\Api\Data\AnnuityInterface;
use Resursbank\Partpayment\Model\AnnuityFactory;
use function is_int;
use function is_string;

/**
 * Convert annuity data from the Resurs Bank API to data which can be
 * interpreted by our Magento module.
 */
class Converter extends AbstractHelper
{
    /**
     * @var AnnuityFactory
     */
    private AnnuityFactory $annuityFactory;

    /**
     * @param Context $context
     * @param AnnuityFactory $annuityFactory
     */
    public function __construct(
        Context $context,
        AnnuityFactory $annuityFactory
    ) {
        $this->annuityFactory = $annuityFactory;

        parent::__construct($context);
    }

    /**
     * @var string
     */
    public const KEY_DURATION = AnnuityInterface::DURATION;

    /**
     * @var string
     */
    public const KEY_FACTOR = AnnuityInterface::FACTOR;

    /**
     * @var string
     */
    public const KEY_TITLE = 'paymentPlanName';

    /**
     * @param array<mixed> $data
     * @param PaymentMethodInterface $method
     * @return AnnuityInterface
     * @throws JsonException
     * @throws ValidatorException
     */
    public function convert(
        array $data,
        PaymentMethodInterface $method
    ): AnnuityInterface {
        // Validate provided data.
        if (!$this->validate($data)) {
            throw new ValidatorException(
                __(
                    'Data conversion failed. Provided data is invalid. %1',
                    json_encode($data, JSON_THROW_ON_ERROR)
                )
            );
        }

        // Validate provided payment method.
        if ($method->getMethodId() === null) {
            throw new ValidatorException(__('Missing payment method entity.'));
        }

        // Convert to data Magento can interpret.
        return $this->annuityFactory->create()->setData([
            AnnuityInterface::METHOD_ID => $method->getMethodId(),
            AnnuityInterface::DURATION => $data[self::KEY_DURATION],
            AnnuityInterface::FACTOR => (float) $data[self::KEY_FACTOR],
            AnnuityInterface::TITLE => $data[self::KEY_TITLE],
            AnnuityInterface::RAW => json_encode($data, JSON_THROW_ON_ERROR)
        ]);
    }

    /**
     * @param array<mixed> $data
     * @return bool
     */
    public function validate(
        array $data
    ): bool {
        return (
            $this->validateDuration($data) &&
            $this->validateFactor($data) &&
            $this->validateTitle($data)
        );
    }

    /**
     * @param array<mixed> $data
     * @return bool
     */
    public function validateDuration(
        array $data
    ): bool {
        return (
            isset($data[self::KEY_DURATION]) &&
            is_int($data[self::KEY_DURATION]) &&
            $data[self::KEY_DURATION] > 0
        );
    }

    /**
     * @param array<mixed> $data
     * @return bool
     */
    public function validateFactor(
        array $data
    ): bool {
        return (
            isset($data[self::KEY_FACTOR]) &&
            is_numeric($data[self::KEY_FACTOR]) &&
            (float) $data[self::KEY_FACTOR] > 0.0
        );
    }

    /**
     * @param array<mixed> $data
     * @return bool
     */
    public function validateTitle(
        array $data
    ): bool {
        return (
            isset($data[self::KEY_TITLE]) &&
            is_string($data[self::KEY_TITLE]) &&
            $data[self::KEY_TITLE] !== ''
        );
    }
}
