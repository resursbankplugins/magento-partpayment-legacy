<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Helper;

use Resursbank\Core\Helper\AbstractLog;

class Log extends AbstractLog
{
    /**
     * @inheritDoc
     */
    protected string $loggerName = 'Resursbank Partpayment Log';

    /**
     * @inheritDoc
     */
    protected string $file = 'resursbank_partpayment';
}
