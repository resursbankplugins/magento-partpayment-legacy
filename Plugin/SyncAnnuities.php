<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Plugin;

use Exception;
use Resursbank\Core\Api\Data\PaymentMethodInterface;
use Resursbank\Core\Helper\Log;
use Resursbank\Core\Helper\PaymentMethods;
use Resursbank\Core\Model\Api\Credentials as CredentialsModel;
use Resursbank\Partpayment\Helper\Annuity\Sync as Helper;
use Resursbank\Partpayment\Helper\Method;

/**
 * This plugin executes after each payment method is synced. It will synchronize
 * annuity factors for said payment method.
 */
class SyncAnnuities
{
    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var Helper
     */
    private Helper $helper;

    /**
     * @var Method
     */
    private Method $method;

    /**
     * @param Log $log
     * @param Helper $helper
     * @param Method $method
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        Log $log,
        Helper $helper,
        Method $method
    ) {
        $this->log = $log;
        $this->helper = $helper;
        $this->method = $method;
    }

    /**
     * @param PaymentMethods $subject
     * @param PaymentMethodInterface $result
     * @param PaymentMethodInterface $method
     * @param CredentialsModel $credentials
     * @return PaymentMethodInterface
     * @noinspection PhpUnusedParameterInspection
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function afterSyncMethodData(
        PaymentMethods $subject,
        PaymentMethodInterface $result,
        PaymentMethodInterface $method,
        CredentialsModel $credentials
    ): PaymentMethodInterface {
        try {
            if ($this->method->isEligible($result)) {
                $this->helper->deleteOldEntries(
                    $this->helper->sync($result, $credentials),
                    $result
                );
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }
}
