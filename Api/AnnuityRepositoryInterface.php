<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Api;

use Magento\Framework\Api\SearchCriteriaInterface;
use Resursbank\Partpayment\Api\Data\AnnuityInterface;
use Resursbank\Partpayment\Api\Data\AnnuitySearchResultsInterface;

interface AnnuityRepositoryInterface
{
    /**
     * Save (update / create) entry.
     *
     * @param AnnuityInterface $entry
     * @return AnnuityInterface
     */
    public function save(AnnuityInterface $entry): AnnuityInterface;

    /**
     * Get entry by ID.
     *
     * @param int $annuityId
     * @return AnnuityInterface
     */
    public function get(int $annuityId): AnnuityInterface;

    /**
     * Retrieve entries matching the specified search criteria.
     *
     * @param SearchCriteriaInterface $searchCriteria
     * @return AnnuitySearchResultsInterface
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    ): AnnuitySearchResultsInterface;

    /**
     * Delete entry.
     *
     * @param AnnuityInterface $entry
     * @return bool
     */
    public function delete(AnnuityInterface $entry): bool;

    /**
     * Delete entry by ID.
     *
     * @param int $annuityId
     * @return bool
     */
    public function deleteById(int $annuityId): bool;
}
