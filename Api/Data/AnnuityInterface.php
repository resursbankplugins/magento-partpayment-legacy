<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Api\Data;

interface AnnuityInterface
{
    /**
     * @var string
     */
    public const ANNUITY_ID = 'annuity_id';

    /**
     * @var string
     */
    public const METHOD_ID = 'method_id';

    /**
     * @var string
     */
    public const DURATION = 'duration';

    /**
     * @var string
     */
    public const FACTOR = 'factor';

    /**
     * @var string
     */
    public const TITLE = 'title';

    /**
     * @var string
     */
    public const RAW = 'raw';

    /**
     * @var string
     */
    public const CREATED_AT = 'created_at';

    /**
     * @var string
     */
    public const UPDATED_AT = 'updated_at';

    /**
     * Get ID of annuity.
     *
     * @return int|null
     */
    public function getAnnuityId(): ?int;

    /**
     * Set ID of annuity.
     *
     * @param int|null $annuityId - Use null to create a new entry.
     * @return self
     */
    public function setAnnuityId(?int $annuityId): self;

    /**
     * Get ID of associated payment method.
     *
     * @return int|null
     */
    public function getMethodId(): ?int;

    /**
     * Set ID of associated payment method.
     *
     * @param int $methodId - Use null to create a new entry.
     * @return self
     */
    public function setMethodId(int $methodId): self;

    /**
     * Get annuity duration.
     *
     * @return int|null
     */
    public function getDuration(): ?int;

    /**
     * Set annuity duration.
     *
     * @param int $duration
     * @return self
     */
    public function setDuration(int $duration): self;

    /**
     * Get annuity factor.
     *
     * @return float|null
     */
    public function getFactor(): ?float;

    /**
     * Set annuity factor.
     *
     * @param float $factor
     * @return self
     */
    public function setFactor(float $factor): self;

    /**
     * Get annuity title.
     *
     * @return string|null
     */
    public function getTitle(): ?string;

    /**
     * Set annuity title.
     *
     * @param string $title
     * @return self
     */
    public function setTitle(string $title): self;

    /**
     * Get complete raw API data defining the annuity at Resurs Bank.
     *
     * @return string|null
     */
    public function getRaw(): ?string;

    /**
     * Set complete raw API data defining the annuity at Resurs Bank.
     *
     * @param string $value
     * @return self
     */
    public function setRaw(string $value): self;

    /**
     * Get entry creation time.
     *
     * @return int|null
     */
    public function getCreatedAt(): ?int;

    /**
     * Set entry creation time.
     *
     * @param int $timestamp - Must be a valid MySQL timestamp.
     * @return self
     */
    public function setCreatedAt(int $timestamp): self;

    /**
     * Get entry update time.
     *
     * @return int|null
     */
    public function getUpdatedAt(): ?int;

    /**
     * Set entry update time.
     *
     * @param int $timestamp - Must be a valid MySQL timestamp.
     * @return self
     */
    public function setUpdatedAt(int $timestamp): self;
}
