<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Model;

use function is_int;
use JsonException;
use Magento\Framework\Exception\ValidatorException;
use Magento\Framework\Model\AbstractModel;
use Resursbank\Partpayment\Api\Data\AnnuityInterface;
use Resursbank\Partpayment\Model\ResourceModel\Annuity as Resource;

/**
 * @SuppressWarnings(PHPMD.ExcessiveClassComplexity)
 */
class Annuity extends AbstractModel implements AnnuityInterface
{
    /**
     * Initialize model.
     *
     * @SuppressWarnings(PHPMD.CamelCaseMethodName)
     * @noinspection MagicMethodsValidityInspection
     */
    protected function _construct(): void
    {
        $this->_init(Resource::class);
    }

    /**
     * @inheritDoc
     */
    public function getAnnuityId(): ?int
    {
        $result = $this->getData(self::ANNUITY_ID);

        return $result === null ? null : (int)$result;
    }

    /**
     * @throws ValidatorException
     * @inheritDoc
     */
    public function setAnnuityId(
        ?int $annuityId
    ): AnnuityInterface {
        if (is_int($annuityId) && $annuityId < 0) {
            throw new ValidatorException(__(
                'Annuity ID must be be an integer that\'s more or equal ' .
                'to 0, or null. Use null to create a new database entry.'
            ));
        }

        $this->setData(self::ANNUITY_ID, $annuityId);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getMethodId(): ?int
    {
        $result = $this->getData(self::METHOD_ID);

        return $result === null ? null : (int)$result;
    }

    /**
     * @throws ValidatorException
     * @inheritDoc
     */
    public function setMethodId(
        int $methodId
    ): AnnuityInterface {
        if ($methodId < 0) {
            throw new ValidatorException(__(
                'Method ID value may not be negative.'
            ));
        }

        $this->setData(self::METHOD_ID, $methodId);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getDuration(): ?int
    {
        $result = $this->getData(self::DURATION);

        return $result === null ? null : (int)$result;
    }

    /**
     * @throws ValidatorException
     * @inheritDoc
     */
    public function setDuration(
        int $duration
    ): AnnuityInterface {
        if ($duration < 0) {
            throw new ValidatorException(__(
                'Duration value may not be negative.'
            ));
        }

        $this->setData(self::DURATION, $duration);

        return $this;
    }

    /**
     * @inheritDoc
     * @phpstan-ignore-next-line Incompatible Magento magic getter.
     */
    public function getFactor(): ?float
    {
        $result = $this->getData(self::FACTOR);

        return $result === null ? null : (float) $result;
    }

    /**
     * @throws ValidatorException
     * @inheritDoc
     */
    public function setFactor(
        float $factor
    ): AnnuityInterface {
        if ($factor < 0) {
            throw new ValidatorException(__(
                'Factor value may not be negative.'
            ));
        }

        $this->setData(self::FACTOR, $factor);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getTitle(): ?string
    {
        $result = $this->getData(self::TITLE);

        return $result === null ? null : (string)$result;
    }

    /**
     * @inheritDoc
     */
    public function setTitle(
        string $title
    ): AnnuityInterface {
        $this->setData(self::TITLE, $title);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getRaw(): ?string
    {
        $result = $this->getData(self::RAW);

        return $result === null ? null : (string)$result;
    }

    /**
     * @throws JsonException
     * @inheritDoc
     */
    public function setRaw(
        string $value
    ): AnnuityInterface {
        // We want to store the encoded value but this lets us confirm its JSON.
        json_decode($value, true, 512, JSON_THROW_ON_ERROR);

        $this->setData(self::RAW, $value);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getCreatedAt(): ?int
    {
        $result = $this->getData(self::CREATED_AT);

        return $result === null ? null : (int)$result;
    }

    /**
     * @inheritDoc
     */
    public function setCreatedAt(
        int $timestamp
    ): AnnuityInterface {
        $this->setData(self::CREATED_AT, $timestamp);

        return $this;
    }

    /**
     * @inheritDoc
     */
    public function getUpdatedAt(): ?int
    {
        $result = $this->getData(self::UPDATED_AT);

        return $result === null ? null : (int)$result;
    }

    /**
     * @inheritDoc
     */
    public function setUpdatedAt(
        int $timestamp
    ): AnnuityInterface {
        $this->setData(self::UPDATED_AT, $timestamp);

        return $this;
    }
}
