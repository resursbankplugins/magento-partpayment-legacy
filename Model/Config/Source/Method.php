<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

namespace Resursbank\Partpayment\Model\Config\Source;

use Exception;
use Magento\Framework\Data\OptionSourceInterface;
use Resursbank\Core\Helper\PaymentMethods;
use Resursbank\Core\Helper\Scope;
use Resursbank\Partpayment\Helper\Log;
use Resursbank\Partpayment\Helper\Method as MethodHelper;

class Method implements OptionSourceInterface
{
    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var MethodHelper
     */
    private MethodHelper $method;

    /**
     * @var PaymentMethods
     */
    private PaymentMethods $paymentMethods;

    /**
     * @var Scope
     */
    private Scope $scope;

    /**
     * @param Log $log
     * @param MethodHelper $method
     * @param PaymentMethods $paymentMethods
     */
    public function __construct(
        Log $log,
        MethodHelper $method,
        PaymentMethods $paymentMethods,
        Scope $scope
    ) {
        $this->log = $log;
        $this->method = $method;
        $this->paymentMethods = $paymentMethods;
        $this->scope = $scope;
    }

    /**
     * @return array<array>
     */
    public function toOptionArray(): array
    {
        $result = [
            [
                'value' => '',
                'label' => __('Please select')
            ]
        ];

        foreach ($this->toArray() as $id => $title) {
            $result[] = [
                'value' => $id,
                'label' => $title
            ];
        }

        return $result;
    }

    /**
     * @return array<int, string>
     */
    public function toArray(): array
    {
        $result = [];

        try {
            $methods = $this->paymentMethods->getMethodsByCredentials(
                $this->scope->getId(),
                $this->scope->getType()
            );

            foreach ($methods as $method) {
                if ($this->method->isEligible($method)) {
                    $methodId = $method->getMethodId();
                    $title = $method->getTitle();

                    if ($methodId !== null && $title !== null) {
                        $result[$methodId] = $title;
                    }
                }
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }
}
