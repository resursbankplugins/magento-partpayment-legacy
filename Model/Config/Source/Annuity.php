<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

namespace Resursbank\Partpayment\Model\Config\Source;

use Exception;
use Magento\Framework\Data\OptionSourceInterface;
use Resursbank\Partpayment\Helper\Log;
use Resursbank\Partpayment\Helper\Annuity as AnnuityHelper;
use Resursbank\Partpayment\Helper\Method as MethodHelper;

class Annuity implements OptionSourceInterface
{
    /**
     * @var Log
     */
    private Log $log;

    /**
     * @var AnnuityHelper
     */
    private AnnuityHelper $annuityHelper;

    /**
     * @var MethodHelper
     */
    private MethodHelper $methodHelper;

    /**
     * @param Log $log
     * @param AnnuityHelper $annuityHelper
     * @param MethodHelper $methodHelper
     */
    public function __construct(
        Log $log,
        AnnuityHelper $annuityHelper,
        MethodHelper $methodHelper
    ) {
        $this->log = $log;
        $this->annuityHelper = $annuityHelper;
        $this->methodHelper = $methodHelper;
    }

    /**
     * @return array<array>
     */
    public function toOptionArray(): array
    {
        $result = [
            [
                'value' => '',
                'label' => __('Please select')
            ]
        ];

        foreach ($this->toArray() as $id => $title) {
            $result[] = [
                'value' => $id,
                'label' => $title
            ];
        }

        return $result;
    }

    /**
     * @return array<int, string>
     */
    public function toArray(): array
    {
        $result = [];

        try {
            $method = $this->methodHelper->getMethod(false);

            if ($method !== null) {
                $annuities = $this->annuityHelper->getListByMethod($method);

                foreach ($annuities as $annuity) {
                    $annuityId = $annuity->getAnnuityId();
                    $title = $annuity->getTitle();

                    if ($annuityId !== null && $title !== null) {
                        $result[$annuityId] = $title;
                    }
                }
            }
        } catch (Exception $e) {
            $this->log->exception($e);
        }

        return $result;
    }
}
