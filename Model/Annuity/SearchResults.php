<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Model\Annuity;

use Magento\Framework\Api\SearchResults as FrameworkSearchResults;
use Resursbank\Partpayment\Api\Data\AnnuitySearchResultsInterface;
use Resursbank\Partpayment\Api\Data\AnnuityInterface;
use function is_array;

class SearchResults extends FrameworkSearchResults implements AnnuitySearchResultsInterface
{
    /**
     * Returns a list of database entries as a result of a database search.
     *
     * This method is necessary to suppress warnings and provide better
     * debugging information during development. Without it, an array of
     * Magento\Framework\Api\AbstractExtensibleObject[] will be returned which
     * is not helpful when all we want to retrieve are entries that adhere to
     * the AnnuityInterface.
     *
     * @inheritDoc
     * @return array<AnnuityInterface>
     */
    public function getItems(): array
    {
        /** @var AnnuityInterface[]|mixed $result */
        $result = parent::getItems();

        return is_array($result) ? $result : [];
    }

    /**
     * @inheritDoc
     */
    public function setItems(
        array $items
    ): AnnuitySearchResultsInterface {
        parent::setItems($items);

        return $this;
    }
}
