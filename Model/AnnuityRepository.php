<?php
/**
 * Copyright © Resurs Bank AB. All rights reserved.
 * See LICENSE for license details.
 */

declare(strict_types=1);

namespace Resursbank\Partpayment\Model;

use Exception;
use Magento\Framework\Api\SearchCriteria\CollectionProcessor\FilterProcessor;
use Magento\Framework\Api\SearchCriteriaInterface;
use Magento\Framework\Exception\AlreadyExistsException;
use Magento\Framework\Exception\LocalizedException;
use Magento\Framework\Exception\NoSuchEntityException;
use Resursbank\Partpayment\Api\AnnuityRepositoryInterface;
use Resursbank\Partpayment\Api\Data\AnnuityInterface;
use Resursbank\Partpayment\Api\Data\AnnuityInterfaceFactory;
use Resursbank\Partpayment\Api\Data\AnnuitySearchResultsInterface;
use Resursbank\Partpayment\Api\Data\AnnuitySearchResultsInterfaceFactory;
use Resursbank\Partpayment\Model\ResourceModel\Annuity as ResourceModel;
use Resursbank\Partpayment\Model\ResourceModel\Annuity\CollectionFactory;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class AnnuityRepository implements AnnuityRepositoryInterface
{
    /**
     * @var AnnuityInterfaceFactory
     */
    protected AnnuityInterfaceFactory $annuityFactory;

    /**
     * @var AnnuitySearchResultsInterfaceFactory
     */
    protected AnnuitySearchResultsInterfaceFactory $searchResultsFactory;

    /**
     * @var ResourceModel
     */
    protected ResourceModel $resourceModel;

    /**
     * @var CollectionFactory
     */
    private CollectionFactory $collectionFactory;

    /**
     * @var FilterProcessor
     */
    private FilterProcessor $filterProcessor;

    /**
     * @param ResourceModel $resourceModel
     * @param AnnuityInterfaceFactory $annuityFactory
     * @param AnnuitySearchResultsInterfaceFactory $searchResultsFactory
     * @param CollectionFactory $collectionFactory
     * @param FilterProcessor $filterProcessor
     */
    public function __construct(
        ResourceModel $resourceModel,
        AnnuityInterfaceFactory $annuityFactory,
        AnnuitySearchResultsInterfaceFactory $searchResultsFactory,
        CollectionFactory $collectionFactory,
        FilterProcessor $filterProcessor
    ) {
        $this->resourceModel = $resourceModel;
        $this->annuityFactory = $annuityFactory;
        $this->searchResultsFactory = $searchResultsFactory;
        $this->collectionFactory = $collectionFactory;
        $this->filterProcessor = $filterProcessor;
    }

    /**
     * @inheritDoc
     * @throws AlreadyExistsException
     * @throws Exception
     */
    public function save(
        AnnuityInterface $entry
    ): AnnuityInterface {
        /** @var Annuity $entry */
        $this->resourceModel->save($entry);

        return $entry;
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function delete(
        AnnuityInterface $entry
    ): bool {
        /** @var Annuity $entry */
        $this->resourceModel->delete($entry);

        return true;
    }

    /**
     * @inheritDoc
     * @throws Exception
     * @throws LocalizedException
     * @throws NoSuchEntityException
     */
    public function deleteById(
        int $annuityId
    ): bool {
        return $this->delete($this->get($annuityId));
    }

    /**
     * @inheritDoc
     * @throws NoSuchEntityException
     */
    public function get(
        int $annuityId
    ): AnnuityInterface {
        /** @var Annuity $result */
        $result = $this->annuityFactory->create();

        $this->resourceModel->load($result, $annuityId);

        if (!$result->getId()) {
            throw new NoSuchEntityException(
                __('Unable to find annuity with ID %1', $annuityId)
            );
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function getList(
        SearchCriteriaInterface $searchCriteria
    ): AnnuitySearchResultsInterface {
        $collection = $this->collectionFactory->create();

        $this->filterProcessor->process($searchCriteria, $collection);

        $collection->load();

        return $this->searchResultsFactory->create()
            ->setSearchCriteria($searchCriteria)
            ->setItems($collection->getItems())
            ->setTotalCount($collection->getSize());
    }
}
